// const Readable = require('stream').Readable
const Xml = require('../../app/classes/xml-output-streamer')
const concat = require('concat-stream')

function expectOutput(str, done, fn) {
  var out = concat((data) => {
    if (typeof str=='string')
      expect(data).to.deep.equal(str)
    else
      expect(data).to.match(str)
    done()
  })
  fn(out)
}

describe('xml output streamer', ()=>{
  it('is 6', (done)=>{
    expectOutput('abcd', done, (out)=>{
      out.write('abcd')
      out.end()
    })
  })

  it('nothing results in empty string', (done)=>{
    expectOutput('', done, out=>{
      var xml = new Xml(out)
      out.end()
    })
  })

  it('ampersand gets translated', (done)=>{
    expectOutput('A &amp; B', done, out=>{
      var xml = new Xml(out)
      xml.text('A & B')
      out.end()
    })
  })

  it('push tag gets written', (done)=>{
    expectOutput('<atag a="3" />', done, out=>{
      var xml = new Xml(out)
      xml.pushTag('atag', {a: 3}, false, true)
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })

  it('push tag gets written', (done)=>{
    expectOutput('<atag a="3">', done, out=>{
      var xml = new Xml(out)
      xml.pushTag('atag', {a: 3}, false, false)
      expect(xml.tagLevel).to.equal(1)
      out.end()
    })
  })

  it('pop tag gets written', (done)=>{
    expectOutput('<atag a="3"><btag a="4" b="abc"></btag></atag><ctag c="def"></ctag>', done, out=>{
      var xml = new Xml(out)
      xml.pushTag('atag', {a: 3}, false, false)
      xml.pushTag('btag', {a: 4, b: 'abc'}, false, false)
      xml.popTag()
      xml.popTag()
      xml.pushTag('ctag', {c: 'def'}, false, false)
      xml.popTag()
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })

  it('close all tags works', (done)=>{
    expectOutput('<atag a="3"><btag a="4" b="abc"></btag></atag>', done, out=>{
      var xml = new Xml(out)
      xml.pushTag('atag', {a: 3}, false, false)
      xml.pushTag('btag', {a: 4, b: 'abc'}, false, false)
      xml.closeAllTags()
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })

  it('withTag works', (done)=>{
    expectOutput('<withtag q="99">a &gt; b</withtag>', done, out=>{
      var xml = new Xml(out)
      var txt = 'a > b'
      xml.withTag('withtag', {q: 99}, false, ()=>{
        xml.text(txt)
      })
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })

  it('indenting works', (done)=>{
    expectOutput('\n<withtag q="99">\n  <anothertag>a &gt; b\n  </anothertag>\n</withtag>', done, out=>{
      var xml = new Xml(out, true)
      var txt = 'a > b'
      xml.withTag('withtag', {q: 99}, null, ()=>{
        xml.withTag('anothertag', {}, null, ()=>{
          xml.text(txt)
        })
      })
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })

  it('withStart works', (done)=>{
    // expectOutput(/<?xml version="1.0" encoding="utf-8"?>.<STEP-Production ExportTime/, done, out=>{
    expectOutput(/<\?xml version="1\.0" encoding="utf-8"\?>\n<STEP-Production ExportTime/, done, out=>{
      var xml = new Xml(out)
      xml.withStart(()=>{
        // console.log('in withStart')
        // xml.text('hello')
        // console.log('end withStart')
      })
      expect(xml.tagLevel).to.equal(0)
      out.end()
    })
  })
})
