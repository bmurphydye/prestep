'use strict';

const files = require('./files');
console.log('files: ', files);
console.log(files.serviceCenter.columns);

const XmlOutputStreamer = require('../classes/xml-output-streamer');
const out = new XmlOutputStreamer();

const readline = require('readline');
const fs = require('fs');
const parse = require('csv-parse');
// const transform = require('stream-transform');

const debug = require('debug');
const info = debug('info');
const logger = debug('log');
const logparser = debug('parser');
const logreader = debug('reader');
const logerror = debug('error');

//     run:  DEBUG=* node .


const defaultDirname = 'realdata/';
const defaultFilename = 'STEP_SERVICE_CENTER_LIST.txt'
const defaultDelimiter = '|';


// info('asdf');
// loadDirectory = function(dirname) {
	// fs.readdir(dirname, function(err, filenames) {
		// var startDate = new Date(2015, 7, 1);
		// for (var filename of filenames) {
			// tokens = filename.split('.');
			// date = new Date(tokens[0], tokens[1], tokens[2]);
			// dayNum = Math.round((date-startDate)/86400000);
			// console.log(filename, tokens, dayNum);
		// }
	// });
// }

// generator = function *gen() {
	// var x = 1 + (yield "foo");
	// console.log(x);
// }

info('now read');

const parseit = (runnumber, dirname, filename) => {
  const parser = parse({ delimiter: defaultDelimiter });
  let rowNum = 0;
  let parserRowNum = 1;
  info(runnumber + ': ' +dirname + filename);

  parser.on('readable', function(){
    let row;
    while(row = parser.read()){
      logparser(runnumber + ' (' + ++parserRowNum + '): ' + row);
      // output.push(record);
    }
  });

  parser.on('error', (err) => {
    logerror('       Parser ERROR: ' + err.message);
  });

  parser.on('finish', () => {
    logparser('all done!');
  });

  const reader = readline.createInterface({
    input: fs.createReadStream(dirname + filename)
  });
  reader.on('line', line => {
    if (++rowNum < 5) {
      logreader(runnumber + ' (' + rowNum + '): ' + JSON.stringify(line));
      if (rowNum > 1)
        parser.write(line+'\n');
    }
  });
  reader.on('close', () => {
    parser.end();
  });
  reader.on('error', (err) => { logerror(runnumber+'     Reader ERROR: ' + err.message) })
}

parseit(1, defaultDirname, defaultFilename);

info('\n\n running again ...');
// parseit(2, defaultDirname, defaultFilename);

// loadFile = function(dirname, filename) {
	// fs.createReadStream(dirname + filename).pipe(parser);
// }

// loadFile(defaultDirname, defaultFilename);
// loadDirectory(defaultDirname);
