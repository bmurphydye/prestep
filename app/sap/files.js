'use strict';

const files = {
  serviceCenter: {
    filename: 'STEP_SERVICE_CENTER_LIST.txt',
    columns: [
      ['code', 'string'],
      ['state', 'string'],
      ['city', 'string'],
      ['zip', 'string'],
      ['inactive', 'string'],
      ['tax_id', 'string']
    ]
  }
}

module.exports = files;

