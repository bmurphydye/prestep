'use strict'




// require('dotenv').load()



// import { co } from 'co'
const co      = require('co')
const fs      = require('co-fs')
const R       = require('ramda')
const inflect = require('inflection')
const crypto  = require('crypto')

// const Fantasy = require('ramda-fantasy')
// const Future = Fantasy.Future

const Xml = require('../classes/xml-output-streamer')
const CsvReader = require('../classes/csv-reader')
const filename = 'comm.txt'

const log   = console.log
const logit = R.tap(x=>{console.log(JSON.stringify(x)); return x})

const then = R.invoker(1, 'then')

const digitalHash = x => crypto.createHash('sha1').update(x).digest('base64')

// adapted from https://github.com/ramda/ramda-fantasy/blob/master/docs/Future.md
const futurize = R.curry( (fn, obj) => Future((reject, resolve)=>fn(obj, (err,result)=>err?reject(err):resolve(result))))
const ls = futurize(fs.readdir)
const readTextFile = futurize(R.curryN(3, fs.readFile)(R.__, 'utf-8'))
const joinTextFiles = dir => ls(dir).chain(R.traverse(Future.of, readTextFile)).map(R.join('\n\n-----\n\n'))

// ls = futurize(fs.readdir)
// ls('.').fork(console.log,console.log)

function isTopLevel(row) { return !row[1] || row[0]==row[1] }
const isChildOf = R.curry((parentId, row) => { return parentId==row[1] && parentId!=row[0] })
const topLevels = R.filter(isTopLevel)
function childrenOf(parentId) { return R.filter(isChildOf(parentId)) }

function logfatalerror(err) {
  console.error('error: ', err)
  // console.trace()
  console.error(err.stack)
  process.exit(1)
}

class Visitor {
  constructor(out, defaultMaxRows) {
    this.out = out
    this.defaultMaxRows = defaultMaxRows
  }

  visitHierarchy(allCategories) {
    this.allCategories = allCategories
    this.visitCategoryChildren(null, topLevels(allCategories), 0)
  }

  visitCategoryChildren(category, children, level) {
    R.map(child=>this.visitCategory(child, category, level+1), children)
  }

  visitCategory(category, parent, level) {
    this.visitCategoryChildren(category, childrenOf(category[0])(this.allCategories), level)
  }
}


// expects array of the form: [ [id parentId name] ]
// note: level starts counting at 1

class HierarchyVisitor extends Visitor {
  run() {
    let self = this

    co(function *() {
      let rows = yield self.read()
      self.visitHierarchy(rows)
    }).catch(logfatalerror)
  }

  visitHierarchy(data) {
    let xml = new Xml(null, true)
    this.type = this.type || 'Classification'
    this.roottype = (this.type=='Classification' ? 'Classification 1' : 'Product')
    this.usertypePrefix = this.usertypePrefix || ''
    this.baseID = this.baseID || this.roottype+' root'
    this.baseName = this.baseName || this.baseID
    this.baseOptions = {ID: this.baseID, UserTypeID: this.roottype+' user-type root', Selected: 'false'}
    if (this.baseParentID) {
      this.baseOptions['ParentID'] = this.baseParentID
    }
    this.out = xml
    xml.withOutputFile(this.rootHierarchyID+'.xml', (out)=>{
      xml.withStart(()=>{
        xml.withTag(this.type+'s', {}, null, ()=>{
          xml.withTag(this.type, this.baseOptions, null, ()=>{
            xml.withTag('Name', {}, false, ()=>xml.text(this.baseName))
            xml.withTag(this.type, {ID: this.rootHierarchyID, UserTypeID: this.rootUserTypeID}, null, ()=>{
              xml.withTag('Name', {}, false, ()=>xml.text(this.rootHierarchyName))
              super.visitHierarchy(data)
            })
          })
        })
      }, 'STEP-ProductInformation')
    })
  }

  visitCategory(category, parent, level) {
    let [ id, parId, name ] = category
    let userTypeID = 'Category'
    if (this.userTypeIDs) {
      if (level-1 > this.userTypeIDs)
        throw new Error('too many category levels ('+level+')')
      userTypeID = this.userTypeIDs[level-1]
    }
    userTypeID = this.usertypePrefix + userTypeID
    this.out.withTag(this.type, {ID: userTypeID+'_'+id, UserTypeID: userTypeID}, null, ()=>{
      this.out.withTag('Name', {}, false, ()=>this.out.text(name))
      super.visitCategory(category, parent, level)
    })
  }

}

class AbstractUnspscVisitor extends HierarchyVisitor {
  // UNSPSC columns:
  //   Segment, seg title, Family, fam title, Class, cl title, [Key], Commodity, com title, Def, Synonym
  static run() {
    let obj = new this(null, null)
    obj.run()
  }

  add(codes, code, replace, name) {
    code = this.zeroed(code, replace)
    if (code in codes) {
      return false
    } else {
      let parent
      if (replace=='000000')
        parent = null
      else
        parent = this.zeroed(code, '00'+replace)
      codes[code] = [code, parent, name]
      return true
    }
  }

  zeroed(code, replace) {
    return code.slice(0,8-replace.length) + replace
  }

  *read() {
    let path = process.env.INPUT_DIR
    // let filename = path + 'unspsc.csv'
    let filename = path + 'our-unspsc.csv'
    let reader = new CsvReader(',')
    let codes = {}
    reader.useCsvParse = true
    yield reader.open(filename)
    let rows = yield reader.all()
    console.log(reader.headers)
    rows.forEach(r => {
      let unspsc = r[2]
      this.add(codes, unspsc, '', r[6])
      if (!this.add(codes, unspsc, '00', r[5])) return false
      if (!this.add(codes, unspsc, '0000', r[4])) return false
      this.add(codes, unspsc, '000000', r[3])
    })
    return R.values(codes)
  }
}

class UnspscHierarchyVisitor extends AbstractUnspscVisitor {
  thisClass() {
    return UnspscVisitor;
  }
  run() {
    this.rootHierarchyID = 'UNSPSC_Hierarchy'
    this.rootHierarchyName = 'UNSPSC Hierarchy'
    this.rootUserTypeID = 'UNSPSC_Root'
    this.idPrefix = 'UNSPSC_'
    this.userTypeIDs = ['Segment', 'Family', 'Class', 'Commodity']
    console.log(this.userTypeIDs)

    super.run()
  }
}

class IdwBlueHierarchyVisitor extends AbstractUnspscVisitor {
  thisClass() {
    return IdwHierarchyVisitor;
  }
  run() {
    this.type = 'Product'
    this.baseID = 'Summit_Member_Records'
    this.baseName = 'Summit Member Records'
    this.baseParentID = 'Product hierarchy root'
    this.rootHierarchyID = 'IDW_Member_Records'
    this.rootHierarchyName = 'IDW Member Records'
    this.rootUserTypeID = 'IDW_Hierarchy_Root'
    this.usertypePrefix = 'Idw_'
    this.userTypeIDs = ['Segment', 'Family', 'Class', 'Commodity']

    super.run()
  }
}

class TsBlueHierarchyVisitor extends AbstractUnspscVisitor {
  thisClass() {
    return TsHierarchyVisitor;
  }
  run() {
    this.type = 'Product'
    this.baseID = 'Summit_Member_Records'
    this.baseName = 'Summit Member Records'
    this.baseParentID = 'Product hierarchy root'
    this.rootHierarchyID = 'TS_Member_Records'
    this.rootHierarchyName = 'TS Member Records'
    this.rootUserTypeID = 'TS_Hierarchy_Root'
    this.usertypePrefix = 'TS_'
    this.userTypeIDs = ['Segment', 'Family', 'Class', 'Commodity']

    super.run()
  }
}

class TsVisitor extends HierarchyVisitor {
  *visitProducts(all=false) {
    let path = process.env.TS_INPUT_DIR
    let filename = path + 'item.txt'
    let reader = new CsvReader('\t')
    let numProducts = 0
    let prod

    yield reader.open(filename)
    while (!reader.isEof()) {
      numProducts = numProducts + 1
      if (this.defaultMaxRows && numProducts > this.defaultMaxRows)
        return
      prod = yield reader.next()
      yield this.visitProduct(prod, reader.getHeaders())
    }
  }

  *visitProduct(product, headers) {
  }
}


class TsLeafClassHierarchy extends TsVisitor {
  static run() {
    let obj = new TsLeafClassHierarchy(null, null)
    obj.run()
  }

  run() {
    this.rootHierarchyID = 'TS_Leaves'
    this.rootHierarchyName = 'Trade Service Leaf Hierarchy'
    this.rootUserTypeID = 'TS_LeafRoot'
    this.idPrefix = 'TsLeaf-'
    super.run()
  }

  constructor(out, defaultMaxRows) {
    super(out, defaultMaxRows)
    this.leafClasses = {}
  }

  *read() {
    let leaves = yield this.getLeaves()
    let categories = {}
    for (let name of leaves) {
      let upper = name.toUpperCase()
      let firstChar = upper[0]
      let id = digitalHash(upper)
      if (!categories[firstChar])
        categories[firstChar] = [firstChar, null, firstChar]
      categories[id] = [id, firstChar, name]
    }
    return R.values(categories)
  }

  *getLeaves() {
    yield this.visitProducts()   // this is called for the side effect of extracting all the leaf classes
    return this.getLeafClasses()
  }

  getLeafClasses() {
    return R.values(this.leafClasses).sort()
  }

  *visitProduct(product, headers) {
    let leaf = R.lensIndex(27)
    let name = R.view(leaf, product)
    let uppername = name.toUpperCase()
    if (name && !this.leafClasses[uppername]) {
      this.leafClasses[uppername] = inflect.titleize(name)
    }
  }
}


class TsCategoryHierarchy extends TsVisitor {
  static run() {
    let obj = new TsCategoryHierarchy(null, null)
    obj.run()
  }

  run() {
    this.rootHierarchyID = 'TS_Hierarchy'
    this.rootHierarchyName = 'Trade Service Hierarchy'
    this.rootUserTypeID = 'TS_Root'
    this.idPrefix = 'TsCategory'
    super.run()
  }

  *read () {
    let file, rows, hash
    let getrows = R.split('\r\n')
    let getcols = R.split('\t')
    let hasdata = function(x){return x.length}
    let path = process.env.TS_INPUT_DIR
    // drops first row (has names) and last row (which is plank)
    let getnonempty = R.compose(R.filter(hasdata), R.tail, getrows)
    //   0       1       2     3     4     5        6             7          8
    // pik, action, parent, code, desc, type, legacy?, enhancedCode, enhanced?
    function pickCols(row){ return [row[8], row[2], row[4]] }
    // let pickCols = R.curry(R.compose(R.values, R.pick))([8,0,4]);

    file = yield fs.readFile(path + filename, 'utf8')
    return R.map(R.compose(pickCols, getcols), getnonempty(file))
  }
}

function *leaves() {
  let obj = new TsLeafClassHierarchy()
  obj.defaultMaxRows = 5555
  console.log(obj, Object.getOwnPropertyNames(obj))
  console.log(obj.visitProducts)
  yield obj.visitProducts()
  logit(obj.getLeafClasses())
  // TsLeafClassHierarchy
}

// TsLeafClassHierarchy.run()
// TsCategoryHierarchy.run()
// UnspscHierarchyVisitor.run()
// IdwBlueHierarchyVisitor.run()
TsBlueHierarchyVisitor.run()

