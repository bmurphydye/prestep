'use strict'

const co         = require('co')
const promisify  = require('promisify-node')
const lineReader = promisify('line-reader')
const R          = require('ramda')
const csvParse      = require('csv-parse')

function logfatalerror(err) {
  console.error('error: ', err)
  // console.trace()
  console.error(err.stack)
  process.exit(1)
}

// const openReader = promisify(lineReader.open)


class CsvReader {
  constructor(colDelimiter=',', isFirstLineHeader=true, rowDelimiter='\n') {
    this.colDelimiter = colDelimiter
    this.isFirstLineHeader = isFirstLineHeader
    this.rowDelimiter = rowDelimiter
    this.parseOptions = {}
    this.useCsvParse = false

    this.rowNum = 0
    this.headers = null
  }

  setParseOptions(opts) {
    this.parseOptions = opts
    this.useCsvParse = true
  }

  getHeaders() { return this.headers }
  // get headers() { return this._headers }
  // set headers(h) { this._headers = h }

  openPromise(filename) {
    this.filename = filename
    return new Promise((resolve, reject) => {
      lineReader.open(filename, (err, reader) =>{
        if (err) reject(err)
        resolve(reader)
      })
    })
  }

  nextLinePromise() {
    return new Promise((resolve, reject) => {
      this.reader.nextLine((err, line) =>{
        if (err) resolve(-1)
        if (line=='') resolve(null)
        if (this.useCsvParse) {
          csvParse(line, this.parseOptions, (err, result)=>{
            if (err) {
              console.log('error in nextLinePromise: ', err)
              reject(err)
            }
            if (result) {
              resolve(result[0])
            } else {
              console.log('empty line')
              resolve(null)
            }
          })
        } else {
          resolve(line.split(this.colDelimiter))
        }
        // resolve(this.parse(line))
      })
    })
  }

  *open(filename) {
    this.reader = yield this.openPromise(filename)
    return this.reader
  }

  isEof() {
    return !this.reader.hasNextLine()
  }

  *all(maxRows=null) {
    let rows = []
    while (!this.isEof()) {
      var line = yield this.next()
      if (line)
        rows.push(line)
      if (maxRows && rows.length>=maxRows) return rows
    }
    console.log('all done')
    return rows
  }

  *next() {
    let line = yield this.nextLinePromise()
    this.rowNum = this.rowNum + 1
    if (line===-1) {
      console.log('illegal data at row '+this.rowNum)
      throw new Error('illegal data at row '+this.rowNum)
    }
    if (!line) {
      console.log('no data at line ', this.rowNum)
      return null
    }
    if (this.rowNum === 1) {
      this.headers = line
      line = yield this.nextLinePromise()
      this.rowNum = this.rowNum + 1
    }
    return line
  }

  *nextAsObj() {
    let line = yield this.next()
    return R.zipObj(this.headers, line)
  }

  *nextAsObjIndex() {
    let line = yield this.next()
    let headers = this.headers.map((h, i)=>h+'_'+i)
    return R.zipObj(headers, line)
  }
}

if (false) {
  co(function *() {
    console.log('starting')
    let path = process.env.INPUT_DIR
    // let filename = path + 'trade-service/item.txt'
    // let reader = new CsvReader('\t')
    let filename = path + 'unspsc.csv'
    let reader = new CsvReader(',')
    reader.setParseOptions({})
    console.log(yield reader.open(filename))
    console.log(reader.isEof())
    for (let i=0; i<5; i++) {
      console.log(yield reader.nextAsObjIndex())
    }

    console.log('done')
  }).catch(logfatalerror)
}

module.exports = CsvReader
