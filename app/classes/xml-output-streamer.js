'use strict';

const escape = require('escape-html')
const moment = require('moment')
const R      = require('ramda')
const fs     = require('fs')
const path   = require('path')

class XmlOutputStreamer {
  constructor(outputStream, newline, extraIndent) {
    this.out = outputStream
    if (this.out) this.out.write('')
    this.newline = newline || false
    this.extraIndent = extraIndent || 0

    this.defaultNewline = newline
    this.tagLevel = 0

    // will reference tags[tagLevel-1], so need an empty string placeholder at tags[0] to prevent error
    this.tags = ['']
    this.tagsNewline = [newline]
  }

  outputBasepath() {
    return process.env.OUTPUT_DIR
  }

  withOutputFile(filepath, fn) {
    let filename = path.join(this.outputBasepath(), filepath)
    try {fs.unlinkSync(filename)} catch(err) {}
    this.out = fs.createWriteStream(filename)
    this.out.addListener('error', (err)=>{
      console.error('error in withOutputFile: ', err, err.stack)
      throw err
    })
    try {
      fn(this.out)
    } finally {
      this.out.end()
    }
  }

  close() {
    this.out.end()
  }

  closeAllTags() {
    for (let i=this.tagLevel; i>0; i--)
      this.popTag()
  }

  withStart(fn, startTagName='STEP-Production') {
    let context = "EN All USA"
    var str = '<?xml version="1.0" encoding="utf-8"?>'
    var attrs = {
      'ExportTime': moment().format('YYYY-MM-DD HH:mm:ss ZZ'),
      "ExportContext": context,
      "ContextID": context,
      "WorkspaceID": "Main",
      "UseContextLocale": "false"
    }
    this.out.write(str)
    if (!this.newline)
      this.out.write('\n')
    this.withTag(startTagName, attrs, null, fn)
  }

  withTag(tagName, attrs, newline, fn) {
    let closeTagImmediately = false   // should be: closeTag = fn!==null, but too tricky
    if (newline===null)
      newline = this.defaultNewline
    try {
      this.pushTag(tagName, attrs, newline, closeTagImmediately)
      fn()
    } finally {
      if (!closeTagImmediately)
        this.popTag()
    }
  }

  pushTag(tagName, attrs, newline, closeTag) {
    if (newline===null) newline = this.defaultNewline
    this.tagLevel = this.tagLevel + 1
    this.tags.push(tagName)
    this.tagsNewline.push(newline)
    if (this.tagsNewline[this.tagLevel-1] || newline)
      this.writeIndent()
    this.out.write('<')
    this.out.write(tagName)
    R.mapObjIndexed(this.writeAttr.bind(this), attrs)
    if (closeTag) {
      this.out.write(' />')
      this.tagLevel = this.tagLevel - 1
      this.tags = R.init(this.tags)
      this.tagsNewline = R.init(this.tagsNewline)
    } else {
      this.out.write('>')
    }
  }

  popTag() {
    if (this.tagLevel <= 0)
      throw new Error('cannot pop empty tag level')
    if (this.tagsNewline[this.tagLevel])
      this.writeIndent()
    this.out.write('</' + this.tags[this.tagLevel] + '>')
    this.tagLevel = this.tagLevel - 1
    this.tags = R.init(this.tags)
    this.tagsNewline = R.init(this.tagsNewline)
  }

  text(str, newline) {
    this.out.write(escape(str))
  }

  writeIndent() {
    this.out.write('\n')
    for (let i=1; i<this.tagLevel; i++)
      this.out.write('  ')
  }

  writeAttr(val, key) {
    this.out.write(' ' + key + '="' + val.toString() + '"')
  }
}

module.exports = XmlOutputStreamer;

